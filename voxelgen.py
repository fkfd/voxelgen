import numpy as np
import matplotlib.pyplot as plt
from probabilities import PROBS

# Entry point is method `voxelgen`.


def surrounding_coordinates(pivot, size) -> list:
    # given pivot and size, generate list of surrounding coordinates in 3D world.
    # pivot - the single-axis coordinate of the "center" the cube,
    # around which other voxels are spawned:
    # e.g. the pivot of a zero-indexed 5x5x5 cube is 2, and its center is (2, 2, 2).
    # size - the size of the populated cube so far:
    # e.g. the size of the cube taking up voxel coordinate 1, 2, and 3 on each axis is 3
    # and this function returns [(0|4, 1..3, 1..3), (1..3, 0|4, 1..3), (1..3, 1..3, 0|4),
    # (0|4, 0|4, 1..3), (0|4, 1..3, 0|4), (1..3, 0|4, 0|4), (0|4, 0|4, 0|4)],
    # a total of 98 blocks (5**3 - 3**3).
    # NOTE: A|B means A or B (in the natural language sense);
    # A..B means A to B inclusive, or the interval [A, B], if you're so into math.

    # we call the list of surrounding coordinates "the layer".
    reach = int((size + 1) / 2)  # distance to the layer from pivot
    # for a cube whose size is 3, reach = 2;
    # since its pivot is (2, 2, 2), we get 2-2 and 2+2 (0|4).
    layer_coords = [pivot - reach, pivot + reach]
    # coordinates inside the cube
    cube_coords = list(range(pivot + reach)[(pivot - reach + 1):])

    layer = []
    # iteration I: sides
    for lc in layer_coords:
        for cc0 in cube_coords:
            for cc1 in cube_coords:
                layer.extend([(lc, cc0, cc1), (cc0, lc, cc1), (cc0, cc1, lc)])
    # iteration II: edges
    for lc0 in layer_coords:
        for lc1 in layer_coords:
            for cc in cube_coords:
                layer.extend([(lc0, lc1, cc), (lc0, cc, lc1), (cc, lc0, lc1)])
    # iteration III: vortices
    for lc0 in layer_coords:
        for lc1 in layer_coords:
            for lc2 in layer_coords:
                layer.extend([(lc0, lc1, lc2)])

    return layer


def calc_voxel(world, x, y, z, probmap) -> int:
    # this function will tell us what to put into a blank voxel at (x, y, z),
    # by detecting its surrounding voxels in `world`
    # `probmap` is the probabilities in `probabilities.py`.
    scale = world.shape[0]
    side = []
    if x > 0:
        side.append(world[x-1, y, z])
    if x < scale - 1:
        side.append(world[x+1, y, z])
    if y > 0:
        side.append(world[x, y-1, z])
    if y < scale - 1:
        side.append(world[x, y+1, z])
    vertical = [255, 255]
    if z > 0:
        vertical[0] = world[x, y, z-1]
    if z < scale - 1:
        vertical[1] = world[x, y, z+1]

    probs = []
    # probability distributions are stored as a list of three lists,
    # respectively for [0] sides, [1] above, and [2] below,
    # the last two belonging to `vertical`.
    for vox in side:
        # adjacent voxels with stuff in them on the side
        if vox != 255:
            probs.append(probmap[vox][0])

    for n, vox in enumerate(vertical):
        if vox != 255:
            # if the voxel is below us, n = 0
            # we need its `above` probabilities, at index 1
            # vice versa (n = 1, index 2)
            probs.append(probmap[vox][n + 1])

    # mean distribution of all the probabilities
    # if we have {1: 0.4, 2: 0.6} and {1: 0.3, 2: 0.7},
    # the mean distribution is {1: (0.4 + 0.3) / 2 = 0.35, 2: (0.6 + 0.7) / 2 = 0.65}
    # same for more probability dicts
    # we now initialize distr with all values being 0
    distr = {k: 0 for k in range(len(probs[0]))}
    for pr in probs:
        for k, v in enumerate(pr):
            distr[k] += v  # dump all values

    prob_n = len(probs)
    for k in distr.keys():
        distr[k] /= prob_n  # all of them now add up to 1
    # finally, pick one voxel value from weighted random
    return np.random.choice(list(distr.keys()), size=1, p=list(distr.values()))[0]


def visualize(world):
    colors = np.empty(world.shape, dtype=object)
    # the xkcd people are clearly better at naming colors
    # the default `brown` looks like red
    # see: https://xkcd.com/color/rgb
    # Rock
    colors[world == 1] = 'xkcd:grey'
    # Dirt
    colors[world == 2] = 'xkcd:brown'
    # Grass
    colors[world == 3] = 'xkcd:green'

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    img = ax.voxels(world, facecolors=colors)
    plt.savefig('voxels.png')


def voxelgen(scale, probmap, printout=False, visual=False):
    # `world` is the 3d cube that holds all the voxels
    # `scale`, its scalar size, must be a positive odd integer
    world_size = (scale, scale, scale)
    world = np.full(world_size, 255, dtype=np.uint8)

    size = 1
    center = int((world_size[0] - 1) / 2)

    world[center, center, center] = 1

    # generate the world
    while size < scale:
        layer = surrounding_coordinates(center, size)
        for x, y, z in layer:
            world[x, y, z] = calc_voxel(world, x, y, z, probmap)
        size += 2  # expand by 1 on each direction, i.e. l,w,h+=2

    if printout:
        print(world)
    if visual:
        visualize(world)

    return world


if __name__ == '__main__':
    voxelgen(9, PROBS, printout=True, visual=True)
