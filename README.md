# VOXELGEN

## Generating a voxel cube using an adjacent-voxel-deterministic approach

### Abstract

This is, you know, just for fun. Idea and code are released in the public domain.

### Demo

![](https://fkfd.me/static/voxelgen.png)

Performance benchmark:

![](https://fkfd.me/static/voxelgen-benchmark.png)

### Terminologies

- world: the largest cubic container throughout voxelgen. It has directions.
  x, width, left, right;
  y, length, in, out;
  z,height: up, down.
- voxels: think of them as 3D pixels. The universe is made of voxels,
  neatly arranged in a n*n*n cube.
  The value of each voxel is an unsigned 8-bit int (np.uint8):
  0 is air, 1-3 other solids, 254 is water, 255 is void.
- blank: having the value of 255.
- populate: to assign a 0-254 value to a voxel, so that it is no longer blank.
- solid: having the value between 0 and 253, inclusive. Because 254 is water.
- blocks: populated voxels.

### Method

> Note: values in `probabilities.py` make some sense but need further finetuning.

1. construct a blank voxel cube of size `n*n*n` full of 255's, called the "world",
   where `n` is a positive odd integer.
2. manually spawn an initial voxel (0-253 value) at its center.
3. now we have a 1x1x1 populated cube, in a 1x1x1 area.
4. on each direction, expand the area by 1 voxel.
5. the area is now 3x3x3 in size.
6. for each unpopulated (== 255) voxel in the area (i.e. voxels surrounding the center),
   fetch a list of its surrounding blocks that are populated.
   For example, coordinate `(x, y, z)` is surrounded by:
   `[(x+1, y, z), (x-1, y, z), (x, y+1, z), (x, y-1, z), (x, y, z+1), (x, y, z-1)]`
7. for each value a populated voxel can have,
   there's a set of probabilities: see `probabilities.py` for more.
8. we have gathered a probability distribution for each possible solid voxel value.
9. for the aforementioned blank voxel, we can assign to it a random value
   from a weighted list of candidates.
10. go to 6 unless all blank voxels in the area are populated.
11. go to 4 unless we don't have any blank voxels left.
12. Nailed it; we have generated a voxel cube.

### Why

[Mastodon thread](https://mastodon.technology/web/statuses/103906460349425196)

as you can see this is a little markov-ish

because people told me i could make such thing with perlin noise, and i was like "hey what if i do it in markov chains" because there are many people (i.e. at least one) literally obsessed with markov around me

so yeah
